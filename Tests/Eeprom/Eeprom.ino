#include <Arduino.h>
#include <ESP8266WiFi.h>
#include <ESP8266WiFiMulti.h>
#include <ESP8266HTTPClient.h>
#include <EEPROM.h>

ESP8266WiFiMulti WiFiMulti;
bool dataFetched = false;
bool dataWritten = false;

String payload;

void fetchData() {
  if(WiFiMulti.run() == WL_CONNECTED) {
  
    HTTPClient http;
  
    Serial.print("[HTTP] begin...\n");
    http.begin("http://www.two-keys.org/bikedisplay/data.txt");
  
    Serial.print("[HTTP] GET...\n");
    int httpCode = http.GET();
  
    // httpCode will be negative on error
    if(httpCode > 0) {
      // HTTP header has been send and Server response header has been handled
      Serial.printf("[HTTP] GET... code: %d\n", httpCode);
  
      // file found at server
      if(httpCode == HTTP_CODE_OK) {
        payload = http.getString();
        Serial.println(payload);
        dataFetched = true;
      }
    }
    else {
      Serial.printf("[HTTP] GET... failed, error: %s\n", http.errorToString(httpCode).c_str());
    }
  
    http.end();
  }
}


void setup() {
  Serial.begin(115200);

  for(uint8_t t = 4; t > 0; t--) {
    Serial.printf("[SETUP] WAIT %d...\n", t);
    Serial.flush();
    delay(1000);
  }

  WiFi.mode(WIFI_STA);
  WiFiMulti.addAP("UPC1000", "SAALUCWQa0");
  WiFiMulti.addAP("TwoKeys", "0123456789");
}

void loop() {
  if (!dataFetched) {
    fetchData();
  }
  else if (!dataWritten) {
    EEPROM.put(0, payload);

    String text;
    EEPROM.get(0, text);

    Serial.println("====");
    Serial.println(text);
    dataWritten = true;
  }

}


