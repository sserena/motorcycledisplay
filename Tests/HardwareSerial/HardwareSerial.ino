#include <HardwareSerial.h>

HardwareSerial hSerial = HardwareSerial(2);

void setup() {
  Serial.begin(115200);
  hSerial.begin(115200);
}

void loop() {
  // put your main code here, to run repeatedly:

  Serial.println("---");
  if (hSerial.available()) {
    char c = hSerial.read();
    Serial.print(c);
  }
  delay(500);
}
