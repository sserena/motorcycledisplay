#include <TFT_eSPI.h> // Graphics and font library for ILI9341 driver chip
#include <SPI.h>

TFT_eSPI tft = TFT_eSPI();  // Invoke library, pins defined in User_Setup.h

void setup() {
  Serial.begin(115200);
  tft.init();
  tft.setRotation(1);

  tft.fillScreen(TFT_BLACK);
  tft.setTextFont(4);
  tft.setTextSize(1);
  tft.setTextColor(TFT_WHITE, TFT_BLACK);
  tft.setTextWrap(false);

  tft.setCursor(3, 3);
  tft.println("Hello World!");
  delay(1000);
  tft.setCursor(3, 3);
  tft.println("Hello World! 1");
  delay(1000);
  tft.setCursor(3, 3);
  tft.println("Hello World! 2");
  delay(1000);
  tft.setCursor(3, 3);
  tft.println("Hello World! 3");
}

void loop() {

  tft.setRotation(1);

  tft.fillScreen(TFT_BLACK);
  tft.setTextFont(4);
  tft.setTextSize(1);
  tft.setTextColor(TFT_WHITE, TFT_BLACK);
  tft.setTextWrap(false);

  tft.setCursor(3, 3);
  tft.println("Hello World!");
  delay(1000);
  tft.setCursor(3, 3);
  tft.println("Hello World! 1");
  delay(1000);
  tft.setCursor(3, 3);
  tft.println("Hello World! 2");
  delay(1000);
  tft.setCursor(3, 3);
  tft.println("Hello World! 3");
  Serial.println("Printing...");
  delay(1000);
}
