#include <TFT_eSPI.h>
#include <SPI.h>

#define DISPLAY_COLOR 0x0D9F
#define PI 3.14159265

TFT_eSPI screen = TFT_eSPI();

struct Point {
  double x;
  double y;
};

int convCoord(int coord, char direction, int size, float rotation) {
  
}

Point rot(double angle, Point point) {
  point.x = point.x - 100;
  point.y = point.y - 100;
  Point newPoint = {
    .x = cos(angle)*point.x - sin(angle)*point.y,
    .y = sin(angle)*point.x + cos(angle)*point.y
  };
  newPoint.x = newPoint.x + 100;
  newPoint.y = newPoint.y + 100;
  return newPoint;
}

void setup() {

  Serial.begin(115200);
  
  screen.init();
  screen.setRotation(1);
  screen.fillScreen(0x0000);

  //screen.drawLine(50, 50, 100, 100, DISPLAY_COLOR);


  int x = 0;
  int y = 100;
  int DISPLAY_ICON_SIZE = 100;

  float xyRatio = 1.5;
  float wingsOffset = 0.3;
  float angle = PI/4;

  float wingsOffsetPx = DISPLAY_ICON_SIZE * wingsOffset;
  int top = y - DISPLAY_ICON_SIZE;
  int right = x + DISPLAY_ICON_SIZE;
  float wingLength = sqrt(2*sq(wingsOffsetPx));
  float wingWidth = wingLength / xyRatio;
  float wingOffsetX = cos(angle)*wingWidth;
  float wingOffsetY = sin(angle)*wingWidth;

  float mainWidth = sqrt(2*sq((right-wingOffsetX) - (x+wingsOffsetPx+wingOffsetX)));
  float mainLength = mainWidth * xyRatio;
  float mainLengthOffset = (mainLength - wingLength) / 2;
  float mainOffsetX = cos(angle)* mainLengthOffset;
  float mainOffsetY = sin(angle)* mainLengthOffset;

  float mirrorRadius = DISPLAY_ICON_SIZE * wingsOffset / 2;
  float mirrorCenterX = x + wingOffsetX - mainOffsetX + (cos(angle) * mainWidth / 2) - (cos(angle)* mirrorRadius);
  float mirrorCenterY = top + wingsOffsetPx + wingOffsetY + mainOffsetY + (sin(angle) * mainWidth / 2) + (sin(angle) * mirrorRadius);

  // First wing
  screen.drawLine(x, top+wingsOffsetPx, x+wingsOffsetPx, top, DISPLAY_COLOR);
  screen.drawLine(x+wingsOffsetPx, top, x+wingsOffsetPx+wingOffsetX, top+wingOffsetY, DISPLAY_COLOR);
  //screen.drawLine(x+wingsOffsetPx+wingOffsetX, top+wingOffsetY, x+wingOffsetX, top+wingsOffsetPx+wingOffsetY, DISPLAY_COLOR);
  screen.drawLine(x, top+wingsOffsetPx, x+wingOffsetX, top+wingsOffsetPx+wingOffsetY, DISPLAY_COLOR);

  // Second wing
  screen.drawLine(right-wingsOffsetPx, y, right, y-wingsOffsetPx, DISPLAY_COLOR);
  screen.drawLine(right, y-wingsOffsetPx, right-wingOffsetX, y-wingsOffsetPx-wingOffsetY, DISPLAY_COLOR);
  screen.drawLine(right-wingsOffsetPx, y, right-wingsOffsetPx-wingOffsetX, y-wingOffsetY, DISPLAY_COLOR);

  // Body
  screen.drawLine(x+wingOffsetX-mainOffsetX, top+wingsOffsetPx+wingOffsetY+mainOffsetY, x+wingsOffsetPx+wingOffsetX+mainOffsetX, top+wingOffsetY-mainOffsetY, DISPLAY_COLOR);
  screen.drawLine(right-wingsOffsetPx-wingOffsetX-mainOffsetX, y-wingOffsetY+mainOffsetY, right-wingOffsetX+mainOffsetX, y-wingsOffsetPx-wingOffsetY-mainOffsetY, DISPLAY_COLOR);
  screen.drawLine(x+wingOffsetX-mainOffsetX, top+wingsOffsetPx+wingOffsetY+mainOffsetY, right-wingsOffsetPx-wingOffsetX-mainOffsetX, y-wingOffsetY+mainOffsetY, DISPLAY_COLOR);
  screen.drawLine(x+wingsOffsetPx+wingOffsetX+mainOffsetX, top+wingOffsetY-mainOffsetY, right-wingOffsetX+mainOffsetX, y-wingsOffsetPx-wingOffsetY-mainOffsetY, DISPLAY_COLOR);

  screen.drawCircle(mirrorCenterX, mirrorCenterY, mirrorRadius, DISPLAY_COLOR);

  // Arrow (rotating)
  /*
  screen.drawLine(50, 100, 100, 50, DISPLAY_COLOR);
  screen.drawLine(100, 50, 150, 100, DISPLAY_COLOR);
  screen.drawLine(150, 100, 120, 100, DISPLAY_COLOR);
  screen.drawLine(120, 100, 120, 150, DISPLAY_COLOR);
  screen.drawLine(120, 150, 80, 150, DISPLAY_COLOR);
  screen.drawLine(80, 150, 80, 100, DISPLAY_COLOR);
  screen.drawLine(80, 100, 50, 100, DISPLAY_COLOR);

  
  Point p1 = {
    .x = 50.0,
    .y = 100.0
  };
  Point p2 = {
    .x = 100.0,
    .y = 50.0
  };
  Point p1Rot = rot(PI/5, p1);
  Point p2Rot = rot(PI/5, p2);
  screen.drawLine(p1Rot.x, p1Rot.y, p2Rot.x, p2Rot.y, 0xB800);

  p1 = {
    .x = 100.0,
    .y = 50.0
  };
  p2 = {
    .x = 150.0,
    .y = 100.0
  };
  p1Rot = rot(PI/5, p1);
  p2Rot = rot(PI/5, p2);
  screen.drawLine(p1Rot.x, p1Rot.y, p2Rot.x, p2Rot.y, 0xB800);
  */
  /*
  // Speed
  screen.drawCircle(100, 100, 50, DISPLAY_COLOR);
  screen.fillRect(50, 121, 100, 30, 0x0000);
  screen.drawLine(54, 120, 146, 120, DISPLAY_COLOR);
  screen.drawCircle(100, 100, 10, DISPLAY_COLOR);
  screen.drawLine(105, 91, 118, 70, DISPLAY_COLOR);
  */

  

  /*
   * Circle: 0,0,50,color
   * FillRect: -50, -20, 100, -30, color
   * Line: -46, -20, 46, -20, color
   * 
   * Circle: 0, 0, 15, color
   * Line: 5, 14, 10, 40, color
   * 
   */
}

void loop() {


}
