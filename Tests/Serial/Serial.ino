#include <HardwareSerial.h>
HardwareSerial gpsPort(1);

void setup() {
  gpsPort.begin(38400, SERIAL_8N1, 16, 17);
  Serial.begin(115200);
}

void loop() {
  while(gpsPort.available()) {
    Serial.write(gpsPort.read());
  }
  while(Serial.available()) {
    gpsPort.write(Serial.read());
  }

}
