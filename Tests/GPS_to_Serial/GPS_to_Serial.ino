#include <HardwareSerial.h>
#include <TinyGPS++.h>
#include <TFT_eSPI.h> // Graphics and font library for ILI9341 driver chip
#include <SPI.h>

TFT_eSPI tft = TFT_eSPI();  // Invoke library, pins defined in User_Setup.h
HardwareSerial gpsPort(2);





void setup() {
  Serial.begin(115200);
  gpsPort.begin(9600, SERIAL_8N1, 16, 17);
  Serial.println("Serial works");

  tft.init();
  tft.setRotation(1);

  tft.fillScreen(TFT_BLACK);
  tft.setTextFont(4);
  tft.setTextSize(1);
  tft.setTextColor(TFT_WHITE, TFT_BLACK);
  
  tft.setCursor(3, 3);
  tft.println("Check Serial...");
}

void loop() {
  while (gpsPort.available() > 0) {
    Serial.write(gpsPort.read());
  }
}
