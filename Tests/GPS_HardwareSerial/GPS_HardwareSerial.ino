#include <HardwareSerial.h>
#include <TinyGPS++.h>
#include <SPI.h>
#include <TFT_eSPI.h>

TFT_eSPI screen = TFT_eSPI();

HardwareSerial gpsPort(1);
TinyGPSPlus gps;


struct Position {
  int x;
  int y;
};

struct DisplayField {
  char* identifier;
  int line;
  int col;
  int width;
  char* suffix;
};


////////////////////////////////
// Settings ////////////////////
////////////////////////////////

// Display
#define DISPLAY_BACKGROUND TFT_BLACK
#define DISPLAY_FONTCOLOR 0x0D9F
#define DISPLAY_FONT 4
#define DISPLAY_FONTSIZE 1
#define DISPLAY_ROTATION 1

#define DISPLAY_MARGIN 10
#define DISPLAY_LINEHEIGHT 40
#define DISPLAY_COLUMNWIDTH 150

DisplayField displayFields[] = {
  {
    .identifier = "speed",
    .line = 0,
    .col = 0,
    .width = DISPLAY_COLUMNWIDTH,
    .suffix = " Speed"
  },
  {
    .identifier = "alt",
    .line = 1,
    .col = 0,
    .width = DISPLAY_COLUMNWIDTH,
    .suffix = " Alt"
  },
  {
    .identifier = "course",
    .line = 2,
    .col = 0,
    .width = DISPLAY_COLUMNWIDTH,
    .suffix = " Course"
  },
  {
    .identifier = "sat",
    .line = 0,
    .col = 1,
    .width = DISPLAY_COLUMNWIDTH,
    .suffix = " Sat"
  },
  {
    .identifier = "hdop",
    .line = 1,
    .col = 1,
    .width = DISPLAY_COLUMNWIDTH,
    .suffix = " Hdop"
  }
};

// MPU
#define MPU_G 9.80665
#define MPU_BALANCETHREASHOLD 1.04

void displayInit() {
  screen.init();
  screen.setRotation(DISPLAY_ROTATION);
  screen.fillScreen(DISPLAY_BACKGROUND);
  screen.setTextFont(DISPLAY_FONT);
  screen.setTextSize(DISPLAY_FONTSIZE);
  screen.setTextColor(DISPLAY_FONTCOLOR, DISPLAY_BACKGROUND);
  screen.setTextWrap(false);
}

void displayPrint(char* identifier, float value) {

  for (int i = 0; i < sizeof(displayFields)/sizeof(displayFields[0]); i++) {
    if (strcmp(identifier, displayFields[i].identifier) == 0) {

      // Get position
      Position pos;
      displayGetPosition(pos, displayFields[i].col, displayFields[i].line);

      // Prepare value
      char valueChar[10];
      sprintf(valueChar, "%.02f", value);
      int totalLength = strlen(valueChar) + strlen(displayFields[i].suffix);
      char printValue[totalLength];
      strcpy(printValue, valueChar);
      strcat(printValue, displayFields[i].suffix);

      // Print value
      screen.setCursor(pos.x, pos.y);
      screen.print(printValue);
      screen.print("   ");

    }
  }
  

}

void displayGetPosition(Position &pos, int col, int line) {
  pos.x = DISPLAY_MARGIN + (col * DISPLAY_COLUMNWIDTH);
  pos.y = DISPLAY_MARGIN + (line * DISPLAY_LINEHEIGHT);
}

void setup() {
  Serial.begin(115200);
  gpsPort.begin(38400, SERIAL_8N1, 17, 16);

  // Display
  displayInit();
}

void loop() {
  while (gpsPort.available() > 0) {
    if (gps.encode(gpsPort.read())) {
      if (gps.altitude.isValid()) {
        Serial.print("Alt: ");
        Serial.println(gps.altitude.meters());
        displayPrint("alt", gps.altitude.meters());
      }
      if (gps.satellites.isValid()) {
        Serial.print("Satellites: ");
        Serial.println(gps.satellites.value());
        displayPrint("sat", gps.satellites.value());
      }
      if (gps.speed.isValid()) {
        Serial.print("Speed: ");
        Serial.println(gps.speed.kmph());
        displayPrint("speed", gps.speed.kmph());
      }
      if (gps.course.isValid()) {
        Serial.print("Course: ");
        Serial.println(gps.course.deg());
        displayPrint("course", gps.course.deg());
      }
      if (gps.hdop.isValid()) {
        Serial.print("HDOP: ");
        Serial.println(gps.hdop.hdop());
        displayPrint("hdop", gps.hdop.hdop());
      }
    }

  }
      
}



/*
#include <HardwareSerial.h>

HardwareSerial GPS(1);

void setup() {
  Serial.begin(115200);
  GPS.begin(57600, SERIAL_8N1, 17, 16);
}

void loop() {
  while (GPS.available()) {
    Serial.print(char(GPS.read()));
  }

}
*/
