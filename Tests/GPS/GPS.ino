#include <SoftwareSerial.h>
#include <NMEAGPS.h>

SoftwareSerial gpsPort(17, false, false, 256);
static NMEAGPS gps;
static gps_fix fix;

#define GPS_PORT_NAME "SoftwareSerial"
#define DEBUG_PORT Serial


void setup() {
  Serial.begin(115200);
  Serial.println("Serial works");
  gpsPort.begin(57600);


}

void loop() {
  
  while (gps.available( gpsPort )) {
    fix = gps.read();
    if (fix.valid.time) {
      Serial.print(fix.dateTime.hours);
      Serial.print(":");
      Serial.print(fix.dateTime.minutes);
      Serial.print(":");
      Serial.println(fix.dateTime.seconds);
    }

    if (fix.valid.location) {
      Serial.print(fix.latitudeL());
      Serial.print(", ");
      Serial.println(fix.longitudeL());

    }
    
    if (fix.valid.altitude) {
      Serial.println(fix.altitude());
    }

    if (fix.valid.speed) {
      Serial.println(fix.speed_kph());
    }

    if (fix.valid.heading) {
      Serial.println(fix.heading());
    }

    Serial.println(fix.satellites);
    
    Serial.println("===============");
  }
}


