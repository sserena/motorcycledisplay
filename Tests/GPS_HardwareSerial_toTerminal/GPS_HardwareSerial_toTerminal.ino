#include <HardwareSerial.h>
#include <TinyGPS++.h>

HardwareSerial gpsPort(2);
TinyGPSPlus gps;




void setup() {
  Serial.begin(115200);
  gpsPort.begin(9600, SERIAL_8N1, 16, 17);
  Serial.println("Serial works");
}

void loop() {
  while (gpsPort.available() > 0) {
    if (gps.encode(gpsPort.read())) {
      if (gps.altitude.isValid()) {
        Serial.print("Alt: ");
        Serial.println(gps.altitude.meters());
      }
      if (gps.satellites.isValid()) {
        Serial.print("Satellites: ");
        Serial.println(gps.satellites.value());
      }
      if (gps.speed.isValid()) {
        Serial.print("Speed: ");
        Serial.println(gps.speed.kmph());
      }
      if (gps.course.isValid()) {
        Serial.print("Course: ");
        Serial.println(gps.course.deg());
      }
      if (gps.hdop.isValid()) {
        Serial.print("HDOP: ");
        Serial.println(gps.hdop.hdop());
      }
    }

  }
      
}
