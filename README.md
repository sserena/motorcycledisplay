# MotorcycleDisplay

This is personal project to add a board computer to my motorcycle. The 3D model is specific to a Bandit 1200 and would likely need to be adjusted for another bike. 

It consists of a NEO6M GPS receiver, a BMP280 thermometer/barometer, a MPU9250 gyroscope/accelerometer/compass as well as an ESP32. A 2.8" TFT is used as a display.

The device currently displays data such as time, speed, altitude, temperature and accelerometer. It can be used as a navigation aid in combination with the script in /Navigation. 

This project was started because I wanted a lean angle indicator for my motorcycle. Unfortunately, this is just about the only feature not yet implemented at all, largely due to the involved math which requires more attention than I've had time for, up to now.