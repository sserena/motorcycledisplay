void mpuCorrectBalance(float *accelX, float *accelY, float *accelZ);

void mpuInit() {
  int status = imu.begin();
  if (status < 0) {
    printStatus("IMU initialization unsuccessful");
    printStatus("Check IMU wiring or try cycling power");
    printStatus("Status: ");
    printStatus(status);
  }
  else {
    mpuInitialized = true;
  }
}

void mpuUpdate() {

  if (!mpuInitialized || !miscCheckInterval("mpu")) {
    return;
  }

  static float accelXDefault;
  static float accelYDefault;
  static float accelZDefault;
  static int measurements = 0;
  static float accelerationMax = 0;
  static float accelerations[10];
  static int index = 0;
  
  imu.readSensor();

  float accelX = imu.getAccelX_mss() / MPU_G;
  float accelY = imu.getAccelY_mss() / MPU_G;
  float accelZ = imu.getAccelZ_mss() / MPU_G;

  // Use the first 50 measurements to find a baseline upon which
  // deviations are computed.
  // TODO: I'm sure there's much better ways to do that...
  if (measurements < 50) {
    accelXDefault += accelX;
    accelYDefault += accelY;
    accelZDefault += accelZ;
    measurements++;
  }
  else if (measurements == 50) {
    accelXDefault /= 50;
    accelYDefault /= 50;
    accelZDefault /= 50;
    measurements++;
  }
  else {

    accelX -= accelXDefault;
    accelY -= accelYDefault;
    accelZ -= accelZDefault;
    
    float accelerationCurrent = cbrtf(accelX*accelX + accelY*accelY + accelZ*accelZ);

    // Normalize
    index = ++index % 10;
    accelerations[index] = accelerationCurrent;
    float totalAcceleration = 0;
    for(float a : accelerations) {
      totalAcceleration += a;
    }
    accelerationCurrent = totalAcceleration / 10;

    if (accelerationCurrent > accelerationMax) {
      accelerationMax = accelerationCurrent;
    }
    
    displayPrint("acceleration_current", accelerationCurrent);
    displayPrint("acceleration_max", accelerationMax);

    // TODO: Calculate angle
    // http://revenanteagle.org/checksix/gyrocam/
    // https://pdfs.semanticscholar.org/f1ca/8f2f33aa6ef7943eef22a2b0440c349655d0.pdf
    // https://www.sciencedirect.com/science/article/pii/S147466701536924X
    displayPrint("angle_current", 12.3);
    displayPrint("angle_max", 26.221);
  }
}
