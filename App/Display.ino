void displayInit() {
  screen.init();
  screen.setRotation(DISPLAY_ROTATION);
  screen.fillScreen(DISPLAY_BACKGROUND); // 0x39C7, DISPLAY_BACKGROUND
  screen.setTextColor(DISPLAY_COLOR, TFT_BLACK);
  screen.setTextWrap(false);
  screen.setTextPadding(0);
  screen.setTextDatum(L_BASELINE);

  screen.drawFastHLine(0, 31, screen.width(), DISPLAY_COLOR);
  screen.drawFastHLine(0, 166, screen.width(), DISPLAY_COLOR);

  // Print default icons and labels
  for (int i = 0; i < sizeof(displayFields)/sizeof(displayFields[0]); i++) {
    if (displayFields[i].icon && displayFields[i].iconUpdates == false) {
      displayFields[i].icon(displayFields[i].x, displayFields[i].y, displayFields[i].iconSize, 0.0f);
    }

    if (strlen(displayFields[i].label)) {
      screen.drawLine(displayFields[i].x, displayFields[i].y, displayFields[i].x, displayFields[i].y-29, DISPLAY_COLOR);
      screen.drawLine(displayFields[i].x+1, displayFields[i].y, displayFields[i].x+1, displayFields[i].y-29, DISPLAY_COLOR);
      screen.setFreeFont(&Nimbus_Sans_L_Regular_12);
      screen.drawString(displayFields[i].label, displayFields[i].x+6, displayFields[i].y-23);
    }
  }  
}

void displayPrint(char* identifier, float value) {
  for (int i = 0; i < sizeof(displayFields)/sizeof(displayFields[0]); i++) {
    if (strcmp(identifier, displayFields[i].identifier) == 0) {

      char valueChar[16];
      sprintf(valueChar, displayFields[i].format, value);
      displayPrint(identifier, valueChar, value);
    }
  }
}

void displayPrint(char* identifier, char* value) {
  displayPrint(identifier, value, 0);
}

void displayPrint(char* identifier, char* value, float originalValue) {

  static TFT_eSprite sprite = TFT_eSprite(&screen);
  
  for (int i = 0; i < sizeof(displayFields)/sizeof(displayFields[0]); i++) {
    if (strcmp(identifier, displayFields[i].identifier) == 0) {
      if (strcmp(value, displayFields[i].value) != 0) {
    
        int x = displayFields[i].x;
        int y = displayFields[i].y;
        int textWidth = 0;
          
        if (!displayFields[i].hideText) {
          
          // Offset within sprite
          if (displayFields[i].icon && displayFields[i].iconUpdates == false) {
            x += displayFields[i].iconSize + displayFields[i].textOffset;
          }
          if (strlen(displayFields[i].label)) {
            x += displayFields[i].textOffset;
          }
  
          // Create new Sprite
          sprite.setColorDepth(16);
          sprite.createSprite(displayFields[i].widthMax, displayFields[i].heightMax);
          sprite.fillSprite(DISPLAY_BACKGROUND); // 0x58C6, DISPLAY_BACKGROUND
  
          // Print Text
          sprite.setTextDatum(L_BASELINE);
          sprite.setTextColor(DISPLAY_COLOR);
          sprite.setFreeFont(&displayFields[i].font);
          sprite.drawString(value, 0, displayFields[i].heightMax-1);
          textWidth += sprite.textWidth(value);
  
          // Suffix
          if (strlen(displayFields[i].suffix)) {
            int offsetX = sprite.textWidth(value) + displayFields[i].suffixOffset;
            sprite.setFreeFont(&displayFields[i].suffixFont);
            sprite.drawString(displayFields[i].suffix, offsetX, displayFields[i].heightMax-1);
  
            textWidth += sprite.textWidth(displayFields[i].suffix);
          }
  
          sprite.pushSprite(x, y - displayFields[i].heightMax);
  
          sprite.deleteSprite();
        }
        else {
          screen.fillRect(x, y-displayFields[i].heightMax, displayFields[i].widthMax, displayFields[i].heightMax, DISPLAY_BACKGROUND);
        }

        // Print Icons
        if (displayFields[i].iconUpdates) {
          displayFields[i].icon(x + textWidth, y, displayFields[i].iconSize, originalValue);
        }
        
        // Save value
        strcpy(displayFields[i].value, value);

      }
    }
  }
}
