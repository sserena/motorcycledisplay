NaviStep naviSteps[50];
int naviStepsAmount;
//double stepDistances[50][NAVI_CHECK_STEPS]; // Guessing that'll always be enough...

// Try to download a new track.
void naviInit() {

  File f;
  bool foundInstructions = false;

  // Check online
  if (startupWifi()) {

    bool instructionsAvailable = true;
    WiFiClient client;

    if (client.connect(NAVI_HOST.c_str(), NAVI_PORT)) {
      
      // Request the file
      client.print(String("GET ") + NAVI_PATH + NAVI_FILE + " HTTP/1.1\r\n" +
                   "Host: " + UPDATE_HOST + "\r\n" +
                   "Cache-Control: no-cache\r\n" +
                   "Connection: close\r\n\r\n");

      // Connect to client to receive bin file
      unsigned long timeout = millis();
      while (client.available() == 0) {
        if (millis() - timeout > 5000) {
          printStatus("No Trip");
          instructionsAvailable = false;
          client.stop();
        }
      }

      
      while (client.available()) {
        String line = client.readStringUntil('\n');
        line.trim();
  
        // if the the line is empty, this is end of headers
        // break the while and download the remaining file.
        if (!line.length()) {
          break;
        }
  
        // Make sure the HTTP Response is 200
        if (line.startsWith("HTTP/1.1")) {
          if (line.indexOf("200") < 0) {
            printStatus("Got a non 200 status code from server.");
            instructionsAvailable = false;
            break;
          }
        }
      }
      if (instructionsAvailable) {
        SPIFFS.begin();
        f = SPIFFS.open(NAVI_FILE, "w");
        while(client.available()) {
          f.write(client.read());
        }
        f.close();
        foundInstructions = true;
      }
    }
  }

  // First, check file system
  if (!foundInstructions) {
    if (SPIFFS.begin()) {
      if (SPIFFS.exists(NAVI_FILE)) {
        foundInstructions = true;
      }
    }
    else {
      SPIFFS.format();
      SPIFFS.begin();
    }
  }



  if (foundInstructions) {
    f = SPIFFS.open(NAVI_FILE, "r");
    if (f.size()) {
      DynamicJsonBuffer jb(20000);
      JsonObject& navigation = jb.parseObject(f);
      if (navigation.success()) {
        float distance = navigation["status"]["distance"];
        float duration = navigation["status"]["duration"];
  
        JsonArray& stepsArray = navigation["steps"];
        naviStepsAmount = stepsArray.size();
  
        //naviSteps = (NaviStep*)malloc(sizeof(NaviStep) * naviStepsAmount);
        int counter = 0;
        for(JsonObject& elm : stepsArray) {
  
          naviSteps[counter].lat = elm["location"]["lat"];
          naviSteps[counter].lng = elm["location"]["long"];
          naviSteps[counter].bearingBefore = elm["bearingBefore"];
          naviSteps[counter].bearingAfter = elm["bearingAfter"];
          //naviSteps[counter].bearings = {1, 2}
          strcpy(naviSteps[counter].instruction, elm["instruction"]);
          strcpy(naviSteps[counter].type, elm["type"]);
          strcpy(naviSteps[counter].address, elm["address"]);
  
          JsonArray& bearings = elm["bearings"];
          int bCounter = 0;
          for(JsonVariant& b : bearings) {
            naviSteps[counter].bearings[bCounter] = b;
            bCounter++;
          }
          for(int i=bCounter; i<5; i++) {
            naviSteps[counter].bearings[i] = -1;
          }
          
          counter++;
        }
      }
      naviEnabled = true;
    }
    f.close();
    SPIFFS.end();
  }
}

void naviUpdate(double lat, double lng, double degrees) {

  static unsigned long lastUpdate = 0;
  static unsigned int currentStep = -1;
  static unsigned long iteration = 0;
  static unsigned long nextUpdateIteration = 0;

  unsigned long timeSinceLastUpdate = millis() - lastUpdate;
  if (timeSinceLastUpdate < NAVI_UPDATE_INTERVAL_GPS_MULTIPLIER * GPS_UPDATE_INTERVAL) {
    return;
  }
  lastUpdate = millis();

  iteration++;
  NaviStepDistance nextStep = {-1, -1};
  naviGetStepDistance(lat, lng, iteration, currentStep, &nextStep);

  // Don't do updates right away if all steps have to be recomputed
  if (nextStep.step != -1) {
    if (currentStep != nextStep.step) {
      currentStep = nextStep.step;
      naviUpdateDisplay(nextStep, lat, lng, degrees, true);
      currentStep = nextStep.step;
    }
    else {
      naviUpdateDisplay(nextStep, lat, lng, degrees, false);
    }
  }
}

void naviUpdateDisplay(NaviStepDistance step, double lat, double lng, double course, bool fullUpdate) {

  if (step.step > -1) {

    if (fullUpdate) {
           
      // Split instructions into lines along word boundaries
      int maxLength = 28;
      char lines[3][maxLength];
      char words[20][20];
      int cc = 0;
      int wc = 0;
      for (int c=0; c<strlen(naviSteps[step.step].instruction); c++) {
        if (naviSteps[step.step].instruction[c] != ' ') {
          words[wc][cc] = naviSteps[step.step].instruction[c];
          cc++;
        }
        else {
          words[wc][cc] = '\0';
          wc ++;
          cc = 0;
        }
      }
      words[wc][cc] = '\0';

      int lineLength = 0;
      int lc = 0;
      for (int c=0; c<=wc; c++) {
        if (strlen(words[c]) + lineLength >= maxLength) {
          lines[lc][lineLength] = '\0';
          lc++;
          lineLength = 0;
        }
        if (lineLength > 0) {
          strcat(lines[lc], " ");
          strcat(lines[lc], words[c]);
          lineLength += strlen(words[c]) + 1;
        }
        else {
          strcpy(lines[lc], words[c]);
          lineLength += strlen(words[c]);
        }
      }
      lines[lc][lineLength] = '\0';
      for (int c=lc+1; c<3; c++) {
        lines[c][0] = '\0';
      }

      // Print
      static TFT_eSprite sprite = TFT_eSprite(&screen);
      
      // Create new Sprite
      sprite.setColorDepth(16);
      sprite.createSprite(220, 62);
      sprite.fillSprite(DISPLAY_BACKGROUND); // 0x58C6, DISPLAY_BACKGROUND

      // Print Text
      sprite.setTextDatum(TL_DATUM);
      sprite.setTextColor(DISPLAY_COLOR);
      sprite.setTextWrap(false);
      sprite.setFreeFont(&Nimbus_Sans_L_Bold_Condensed_18);
      for(int i=0; i<3; i++) {
        sprite.drawString(lines[i], 0, i*22);

      }

      sprite.pushSprite(95, 177);

      sprite.deleteSprite();
    }

    // Distance
    char distance[8];
    if (step.distance < 1000) {
      sprintf(distance, "%.0f", step.distance);
      strcat(distance, "m:");
    }
    else if (step.distance < 100000) {
      sprintf(distance, "%.1f", step.distance/1000);
      strcat(distance, "km:");
    }
    else {
      sprintf(distance, "%.0f", step.distance/1000);
      strcat(distance, "km:");
    }
    displayPrint("navigation_distance", distance);

    // Course
    if (step.distance > 200 || step.step == 0 || step.step == naviStepsAmount - 1) {
      double courseTo = gps.courseTo(lat, lng, naviSteps[step.step].lat, naviSteps[step.step].lng);
      double courseCorrection = courseTo - course;
      displayPrint("navigation_course_correction", courseCorrection);
    }
    else {

      screen.fillRect(12, 193, 56, 237, DISPLAY_BACKGROUND); // 0x58C6, DISPLAY_BACKGROUND
      
      // BearingBefore always goes straight down
      naviDrawBearing(180, true);
      
      int bearingBefore = naviSteps[step.step].bearingBefore;
      int bearingAfter = naviSteps[step.step].bearingAfter;
      naviDrawBearing(bearingAfter-bearingBefore, true);

      for(int i=0; i<5; i++) {
        int b = naviSteps[step.step].bearings[i];
        if (b != -1 && abs(b-(bearingBefore-180 % 360)) > 15 & abs(b-bearingAfter) > 15) {
          Serial.print(b); Serial.print(" "); Serial.println(b-bearingBefore);
          naviDrawBearing(b-bearingBefore, false);
        }
      }

    }
  }
}

void naviDrawBearing(int bearing, bool main) {
  uint16_t secondaryColor = 0x8410;
  int radius = 22;
  int centerX = 34;
  int centerY = 215;
  static float conv = 180/PI;

  int x = centerX + radius * sin(bearing/conv);
  int y = centerY - radius * cos(bearing/conv);
  uint16_t color = (main ? DISPLAY_COLOR : secondaryColor);

  screen.drawLine(centerX, centerY, x, y, color);  
}

// Compute current navigation step as distance to next step
void naviGetStepDistance(float lat, float lng, int iteration, int step, NaviStepDistance *nextStep) {

  static bool firstStep = true;
  static bool firstIteration = true;
  static float oldLat = lat;
  static float oldLng = lng;
  static double deltas[NAVI_CHECK_STEPS];
  
  // Calculate threshold for the avgDistance to matter (standing still will result in unusable data)
  static double distanceThreshold = (double)NAVI_SPEED_THRESHOLD / 3.6 * ((double)NAVI_UPDATE_INTERVAL_GPS_MULTIPLIER * (double)GPS_UPDATE_INTERVAL / 1000.0); // meter per time interval


  // Do that until first step gets further away
  if (firstStep) {

    // At first iteration, only save old lats and lngs (above)
    if (!firstIteration) {

      // Get Delta
      double oldDistance = gps.distanceBetween(oldLat, oldLng, naviSteps[0].lat, naviSteps[0].lng);
      double currentDistance = gps.distanceBetween(lat, lng, naviSteps[0].lat, naviSteps[0].lng);
      double delta = currentDistance - oldDistance; // Negative numbers = getting closer

      double totalDelta = 0;
      for(int m=NAVI_CHECK_STEPS-2; m>=0; m--) {
        deltas[m+1] = deltas[m];
        totalDelta += deltas[m];
      }
      deltas[0] = delta;

      oldLat = lat;
      oldLng = lng;

      if (iteration > NAVI_CHECK_STEPS) {
        totalDelta += delta;
        double avgDistance = totalDelta / NAVI_CHECK_STEPS;
        //Serial.print(avgDistance); Serial.print(" - "); Serial.println(distanceThreshold);
        if (avgDistance > distanceThreshold) {
          firstStep = false;
        }
        else {
          nextStep->step = 0;
          nextStep->distance = currentDistance;
        }
      }
    }
    firstIteration = false;
  }
  else {

    double shortestDistances[2];

    // Check steps - all of them every 10, only next otherwise
    if (iteration % 10 == 0) {

      // Find position
      int bestStep;
      double bestDistance = 999999;
      for(int i=0; i<naviStepsAmount-1; i++) {

        // Compute distance from position to step (in cartesian coordinates... close enough I think)
        double m = (naviSteps[i+1].lat - naviSteps[i].lat)/(naviSteps[i+1].lng - naviSteps[i].lng);
        double b = naviSteps[i].lat - (m * naviSteps[i].lng);
        double distance = fabs(m*lng - lat + b) / sqrt(m*m + 1);
        
        // Get distance to start and end points to make sure we're next to the line
        double distance1 = sqrt(sq(naviSteps[i].lat-lat) + sq(naviSteps[i].lng-lng));
        double distance2 = sqrt(sq(naviSteps[i+1].lat-lat) + sq(naviSteps[i+1].lng-lng));

        // Only continue if we're next to the line
        if (distance <= distance1 && distance <= distance2) {
          if (distance < bestDistance) {
            bestStep = i+1; // We're always comparing the previous to the current step
            bestDistance = distance;
          }
        }
      }

      nextStep->step = bestStep;
      nextStep->distance = gps.distanceBetween(lat, lng, naviSteps[bestStep].lat, naviSteps[bestStep].lng);
    }
    else {
      nextStep->step = step;
      nextStep->distance = gps.distanceBetween(lat, lng, naviSteps[step].lat, naviSteps[step].lng);
    }
  }
}
