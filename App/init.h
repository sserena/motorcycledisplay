////////////////////////////////
// Structs /////////////////////
////////////////////////////////
typedef struct {
  char* identifier;
  int x;
  int y;
  int widthMax;
  int heightMax;
  const char* format;
  const GFXfont font;
  const char* suffix;
  const GFXfont suffixFont;
  int suffixOffset;
  int textOffset;
  const char* label;
  bool hideText;
  int iconSize;
  bool iconUpdates;
  void (*icon)(int x, int y, int iconSize, float value);
  char value[10];
} DisplayField;

typedef struct {
  double lat;
  double lng;
  int bearingBefore;
  int bearingAfter;
  int bearings[5];
  char instruction[100];
  char type[20];
  char address[100];
} NaviStep;

typedef struct {
  int step;
  float distance;
} NaviStepDistance;

////////////////////////////////
// Settings ////////////////////
////////////////////////////////

// General
#define GENERAL_PRINT_STATUS_MESSAGES true

// Wifi
#define WIFI_WAIT_FOR_LOGIN_MS 10000
#define WIFI_SSID "UPC"
#define WIFI_PASSWORD "---"

// Update
String UPDATE_HOST = "www.increations.eu";
String UPDATE_PATH = "/boardcomputer/boardcomputer.bin";
#define UPDATE_PORT 80

// Navi
String NAVI_HOST = "www.increations.eu";
String NAVI_PATH = "/boardcomputer";
#define NAVI_FILE "/trip.json"
#define NAVI_PORT 80
#define NAVI_UPDATE_INTERVAL_GPS_MULTIPLIER 2
#define NAVI_CHECK_STEPS 5
#define NAVI_SPEED_THRESHOLD 20 // km/h

// Display
#define DISPLAY_ROTATION 1

DisplayField displayFields[] = {

  {
    .identifier = "time",
    .x = 4,
    .y = 24,
    .widthMax = 100,
    .heightMax = 19,
    .format = "",
    .font = Nimbus_Sans_L_Bold_Condensed_24,
    .suffix = "",
    .suffixFont = Nimbus_Sans_L_Bold_Condensed_18,
    .suffixOffset = 0,
    .textOffset = 0,
    .label = "",
    .hideText = false,
    .iconSize = 0
  },
  {
    .identifier = "temperature",
    .x = 133,
    .y = 24,
    .widthMax = 70,
    .heightMax = 19,
    .format = "%.1f",
    .font = Nimbus_Sans_L_Bold_Condensed_24,
    .suffix = "c",
    .suffixFont = Nimbus_Sans_L_Bold_Condensed_18,
    .suffixOffset = 4,
    .textOffset = 0,
    .label = "",
    .hideText = false,
    .iconSize = 0
  },
  {
    .identifier = "weather",
    .x = 195,
    .y = 24,
    .widthMax = 0,
    .heightMax = 0,
    .format = "",
    .font = Nimbus_Sans_L_Bold_Condensed_24,
    .suffix = "",
    .suffixFont = Nimbus_Sans_L_Bold_Condensed_18,
    .suffixOffset = 0,
    .textOffset = 0,
    .label = "",
    .hideText = false,
    .iconSize = 0
  },
  {
    .identifier = "satellites",
    .x = 274,
    .y = 24,
    .widthMax = 25,
    .heightMax = 19,
    .format = "%.0f",
    .font = Nimbus_Sans_L_Bold_Condensed_24,
    .suffix = "",
    .suffixFont = Nimbus_Sans_L_Bold_Condensed_18,
    .suffixOffset = 0,
    .textOffset = 7,
    .label = "",
    .hideText = false,
    .iconSize = 16,
    .iconUpdates = false,
    .icon = iconSatellites
  },
  {
    .identifier = "speed_current",
    .x = 11,
    .y = 71,
    .widthMax = 140,
    .heightMax = 28,
    .format = "%.2f",
    .font = Nimbus_Sans_L_Bold_Condensed_36,
    .suffix = "km/h",
    .suffixFont = Nimbus_Sans_L_Bold_Condensed_18,
    .suffixOffset = 7,
    .textOffset = 12,
    .label = "",
    .hideText = false,
    .iconSize = 24,
    .iconUpdates = false,
    .icon = iconSpeedCurent
  },
  {
    .identifier = "speed_average",
    .x = 200,
    .y = 71,
    .widthMax = 45,
    .heightMax = 19,
    .format = "%.0f",
    .font = Nimbus_Sans_L_Bold_Condensed_24,
    .suffix = "",
    .suffixFont = Nimbus_Sans_L_Bold_Condensed_18,
    .suffixOffset = 0,
    .textOffset = 7,
    .label = "avg",
    .hideText = false,
    .iconSize = 0
  },
  {
    .identifier = "speed_max",
    .x = 260,
    .y = 71,
    .widthMax = 45,
    .heightMax = 19,
    .format = "%.0f",
    .font = Nimbus_Sans_L_Bold_Condensed_24,
    .suffix = "",
    .suffixFont = Nimbus_Sans_L_Bold_Condensed_18,
    .suffixOffset = 0,
    .textOffset = 7,
    .label = "max",
    .hideText = false,
    .iconSize = 0
  },
  {
    .identifier = "altitude_current",
    .x = 11,
    .y = 113,
    .widthMax = 140,
    .heightMax = 28,
    .format = "%.1f",
    .font = Nimbus_Sans_L_Bold_Condensed_36,
    .suffix = "m",
    .suffixFont = Nimbus_Sans_L_Bold_Condensed_18,
    .suffixOffset = 7,
    .textOffset = 12,
    .label = "",
    .hideText = false,
    .iconSize = 24,
    .iconUpdates = false,
    .icon = iconAltitudeCurrent
  },
  {
    .identifier = "altitude_total",
    .x = 200,
    .y = 113,
    .widthMax = 85,
    .heightMax = 19,
    .format = "%.0f",
    .font = Nimbus_Sans_L_Bold_Condensed_24,
    .suffix = "",
    .suffixFont = Nimbus_Sans_L_Bold_Condensed_18,
    .suffixOffset = 0,
    .textOffset = 7,
    .label = "total",
    .hideText = false,
    .iconSize = 0
  },
  {
    .identifier = "distance",
    .x = 11,
    .y = 154,
    .widthMax = 140,
    .heightMax = 28,
    .format = "%.1f",
    .font = Nimbus_Sans_L_Bold_Condensed_36,
    .suffix = "km",
    .suffixFont = Nimbus_Sans_L_Bold_Condensed_18,
    .suffixOffset = 7,
    .textOffset = 12,
    .label = "",
    .hideText = false,
    .iconSize = 24,
    .iconUpdates = false,
    .icon = iconDistance
  },
  {
    .identifier = "course",
    .x = 210,
    .y = 154,
    .widthMax = 70,
    .heightMax = 30,
    .format = "%.0f",
    .font = Nimbus_Sans_L_Bold_Condensed_24,
    .suffix = "",
    .suffixFont = Nimbus_Sans_L_Bold_Condensed_24,
    .suffixOffset = 0,
    .textOffset = 0,
    .label = "",
    .hideText = false,
    .iconSize = 20,
    .iconUpdates = true,
    .icon = iconCourse
  },
  {
    .identifier = "angle_current",
    .x = 11,
    .y = 200,
    .widthMax = 220,
    .heightMax = 26,
    .format = "%.0f",
    .font = Nimbus_Sans_L_Bold_Condensed_24,
    .suffix = "",
    .suffixFont = Nimbus_Sans_L_Bold_Condensed_24,
    .suffixOffset = 0,
    .textOffset = 0,
    .label = "",
    .hideText = false,
    .iconSize = 28,
    .iconUpdates = true,
    .icon = iconAngle
  },
  {
    .identifier = "acceleration_current",
    .x = 11,
    .y = 232,
    .widthMax = 220,
    .heightMax = 26,
    .format = "%.1f",
    .font = Nimbus_Sans_L_Bold_Condensed_24,
    .suffix = "g",
    .suffixFont = Nimbus_Sans_L_Bold_Condensed_24,
    .suffixOffset = 0,
    .textOffset = 0,
    .label = "",
    .hideText = false,
    .iconSize = 28,
    .iconUpdates = true,
    .icon = iconAcceleration
  },
  {
    .identifier = "angle_max",
    .x = 257,
    .y = 200,
    .widthMax = 50,
    .heightMax = 19,
    .format = "%.0f",
    .font = Nimbus_Sans_L_Bold_Condensed_24,
    .suffix = "",
    .suffixFont = Nimbus_Sans_L_Bold_Condensed_24,
    .suffixOffset = 0,
    .textOffset = 0,
    .label = "",
    .hideText = false,
    .iconSize = 0,
    .iconUpdates = false
  },
  {
    .identifier = "acceleration_max",
    .x = 257,
    .y = 232,
    .widthMax = 50,
    .heightMax = 19,
    .format = "%.1f",
    .font = Nimbus_Sans_L_Bold_Condensed_24,
    .suffix = "g",
    .suffixFont = Nimbus_Sans_L_Bold_Condensed_24,
    .suffixOffset = 0,
    .textOffset = 0,
    .label = "",
    .hideText = false,
    .iconSize = 0,
    .iconUpdates = false
  },
  {
    .identifier = "navigation_distance",
    .x = 11,
    .y = 190,
    .widthMax = 78,
    .heightMax = 19,
    .format = "%.0f",
    .font = Nimbus_Sans_L_Bold_Condensed_24,
    .suffix = "",
    .suffixFont = Nimbus_Sans_L_Bold_Condensed_24,
    .suffixOffset = 0,
    .textOffset = 0,
    .label = "",
    .hideText = false,
    .iconSize = 0,
    .iconUpdates = false
  },
  {
    .identifier = "navigation_course_correction",
    .x = 5,
    .y = 230,
    .widthMax = 45,
    .heightMax = 35,
    .format = "%.0f",
    .font = Nimbus_Sans_L_Bold_Condensed_24,
    .suffix = "",
    .suffixFont = Nimbus_Sans_L_Bold_Condensed_24,
    .suffixOffset = 0,
    .textOffset = 0,
    .label = "",
    .hideText = true,
    .iconSize = 30,
    .iconUpdates = true,
    .icon = iconCourse
  }
};

// MPU
#define MPU_G 9.80665
#define MPU_BALANCETHREASHOLD 1.04
#define MPU_UPDATE_INTERVAL 50

// GPS
#define GPS_BAUD 38400
#define GPS_UART_RX 16
#define GPS_UART_TX 17
#define GPS_UPDATE_INTERVAL 200
#define GPS_ALTITUDE_INTERVAL 20 // every n iterations
#define GPS_DISTANCE_INTERVAL 10
#define GPS_MEAN_SAMPLES 5

// BME
#define BME_UPDATE_INTERVAL 10000
#define BME_SEA_LEVE_PRESSURE 1013.25
