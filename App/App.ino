#include "FS.h"
#include <TFT_eSPI.h>
#include <SPI.h>
#include <HardwareSerial.h>
#include <TinyGPS++.h>
#include "MPU9250.h"
#include <BME280I2C.h>
#include <Wire.h>
#include <WiFi.h>
#include <Update.h>
#include <ArduinoJson.h>

TFT_eSPI screen = TFT_eSPI();  // Invoke library, pins defined in User_Setup.h
MPU9250 imu(Wire,0x68);
HardwareSerial gpsPort(1);
TinyGPSPlus gps;
BME280I2C bme;

// Define here because it's used in icons.h which has to be defined before init.h (where it belongs).
// TODO: Tidy up...
#define DISPLAY_COLOR 0x0D9F // 0x0D9F (Default) 0xAE9D (Light Blue) 0x70CC (Byzantium - dark) 0x58C6 (Tyrian Purple - very dark)
#define DISPLAY_BACKGROUND 0x0000

#include "icons.h"
#include "fonts.h"
#include "init.h"

bool gpsInitialized = false;
bool bmeInitialized = false;
bool mpuInitialized = false;
bool naviEnabled = false;

void setup() {
  Serial.begin(115200);
  while(!Serial) {}

  naviInit();
  displayInit();
  if (!naviEnabled) {
    mpuInit();
  }
  gpsInit();
  bmeInit();
}

void loop() {
  if (!naviEnabled) {
    mpuUpdate();
  }
  gpsUpdate();
  bmeUpdate();
  delay(10);
}
