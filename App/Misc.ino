bool miscCheckInterval(char *identifier) {

  static unsigned long gps = millis();
  static unsigned long mpu = millis();
  static unsigned long bme = millis();
  
  if (strcmp(identifier, "gps") == 0) {
    if (millis() - gps >= GPS_UPDATE_INTERVAL) {
      gps = millis();
      return true;
    }
    return false;
  }
  else if (strcmp(identifier, "mpu") == 0) {
    if (millis() - mpu >= MPU_UPDATE_INTERVAL) {
      mpu = millis();
      return true;
    }
    return false;
  }
  else if (strcmp(identifier, "bme") == 0) {
    if (millis() - bme >= BME_UPDATE_INTERVAL) {
      bme = millis();
      return true;
    }
    return false;
  }
}

double sum_array(double a[], int num_elements)
{
   int i;
   double sum=0;
   for (i=0; i<num_elements; i++)
   {
   sum = sum + a[i];
   }
   return(sum);
}

void printStatus(const char *msg) {
  printStatus(msg, true);
}

void printStatus(int msg) {
  char cstr[16];
  itoa(msg, cstr, 10);
  printStatus(cstr, true);
}

void printStatus(String msg) {
  printStatus(msg.c_str(), true);
}

void printStatus(const char *msg, bool linebreak) {
  if (GENERAL_PRINT_STATUS_MESSAGES) {
    if (linebreak) {
      Serial.print("*** ");
      Serial.println(msg);
    }
    else {
      Serial.print(msg);
    }
  }
}

void p(double num) {
  Serial.print(num, 10);
}
void pl(double num) {
  Serial.println(num, 10);
}
void p(const char *msg) {
  Serial.print(msg);
}
void pl(const char *msg) {
  Serial.println(msg);
}
