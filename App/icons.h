#define PI 3.14159265

struct Point {
  double x;
  double y;
};

// Rotates a point around an anchor.
Point rot(double angle, int offsetX, int offsetY, Point point) {
  point.x = point.x - offsetX;
  point.y = point.y - offsetY;
  Point newPoint = {
    .x = cos(angle)*point.x - sin(angle)*point.y,
    .y = sin(angle)*point.x + cos(angle)*point.y
  };
  newPoint.x = newPoint.x + offsetX;
  newPoint.y = newPoint.y + offsetY;
  return newPoint;
}

void iconSpeedCurent(int x, int y, int iconSize, float value) {

  int radius = iconSize / 2;
  int height = iconSize / 3 * 2;
  int centerY = y - height / 2;
  int centerX = x + radius;

  int bottomLineOffset = sqrt(sq(radius) - sq(iconSize / 6)) - 2;
  int innerRadius = ceil(radius/5);

  // Finger
  float fingerLength = radius * 0.7;
  float fingerAngle = PI / 3;
  int fingerX1 = centerX + cos(fingerAngle) * innerRadius;
  int fingerY1 = centerY - sin(fingerAngle) * innerRadius;
  int fingerX2 = centerX + cos(fingerAngle) * fingerLength;
  int fingerY2 = centerY - sin(fingerAngle) * fingerLength;

  screen.drawCircle(centerX, centerY, radius, DISPLAY_COLOR);
  screen.fillRect(x, y+1, iconSize, iconSize-height, DISPLAY_BACKGROUND);
  screen.drawLine(centerX-bottomLineOffset, y, centerX+bottomLineOffset, y, DISPLAY_COLOR);
  screen.drawCircle(centerX, centerY, innerRadius, DISPLAY_COLOR);
  screen.drawLine(fingerX1, fingerY1, fingerX2, fingerY2, DISPLAY_COLOR);
  
}

void iconAltitudeCurrent(int x, int y, int iconSize, float value) {
  int centerX = x + iconSize / 2;
  int top = y - iconSize;
  int arrowheadOffset = iconSize / 6;
  
  screen.drawLine(centerX, top, centerX, y, DISPLAY_COLOR);
  screen.drawLine(centerX, top, centerX-arrowheadOffset, top+arrowheadOffset, DISPLAY_COLOR);
  screen.drawLine(centerX, top, centerX+arrowheadOffset, top+arrowheadOffset, DISPLAY_COLOR);
  screen.drawLine(centerX, y, centerX-arrowheadOffset, y-arrowheadOffset, DISPLAY_COLOR);
  screen.drawLine(centerX, y, centerX+arrowheadOffset, y-arrowheadOffset, DISPLAY_COLOR);
}

void iconDistance(int x, int y, int iconSize, float value) {
  int centerY = y - iconSize / 2;
  int right = x + iconSize;
  int arrowheadOffset = iconSize / 6;
  
  screen.drawLine(x, centerY, right, centerY, DISPLAY_COLOR);
  screen.drawLine(x, centerY, x+arrowheadOffset, centerY+arrowheadOffset, DISPLAY_COLOR);
  screen.drawLine(x, centerY, x+arrowheadOffset, centerY-arrowheadOffset, DISPLAY_COLOR);
  screen.drawLine(right, centerY, right-arrowheadOffset, centerY+arrowheadOffset, DISPLAY_COLOR);
  screen.drawLine(right, centerY, right-arrowheadOffset, centerY-arrowheadOffset, DISPLAY_COLOR);

}

void iconSatellites(int x, int y, int iconSize, float value) {
  float xyRatio = 1.5;
  float wingsOffset = 0.3;
  float angle = PI/4;

  int top = y - iconSize; 
  int right = x + iconSize;
  float wingsOffsetPx = iconSize * wingsOffset;
  
  float wingLength = sqrt(2*sq(wingsOffsetPx));
  float wingWidth = wingLength / xyRatio;
  float wingOffsetX = cos(angle)*wingWidth;
  float wingOffsetY = sin(angle)*wingWidth;

  float mainWidth = sqrt(2*sq((right-wingOffsetX) - (x+wingsOffsetPx+wingOffsetX)));
  float mainLength = mainWidth * xyRatio;
  float mainLengthOffset = (mainLength - wingLength) / 2;
  float mainOffsetX = cos(angle)* mainLengthOffset;
  float mainOffsetY = sin(angle)* mainLengthOffset;

  float mirrorRadius = iconSize * wingsOffset / 2;
  float mirrorCenterX = x + wingOffsetX - mainOffsetX + (cos(angle) * mainWidth / 2) - (cos(angle)* mirrorRadius);
  float mirrorCenterY = top + wingsOffsetPx + wingOffsetY + mainOffsetY + (sin(angle) * mainWidth / 2) + (sin(angle) * mirrorRadius);

  // First wing
  screen.drawLine(x, top+wingsOffsetPx, x+wingsOffsetPx, top, DISPLAY_COLOR);
  screen.drawLine(x+wingsOffsetPx, top, x+wingsOffsetPx+wingOffsetX, top+wingOffsetY, DISPLAY_COLOR);
  screen.drawLine(x, top+wingsOffsetPx, x+wingOffsetX, top+wingsOffsetPx+wingOffsetY, DISPLAY_COLOR);

  // Second wing
  screen.drawLine(right-wingsOffsetPx, y, right, y-wingsOffsetPx, DISPLAY_COLOR);
  screen.drawLine(right, y-wingsOffsetPx, right-wingOffsetX, y-wingsOffsetPx-wingOffsetY, DISPLAY_COLOR);
  screen.drawLine(right-wingsOffsetPx, y, right-wingsOffsetPx-wingOffsetX, y-wingOffsetY, DISPLAY_COLOR);

  // Body
  screen.drawLine(x+wingOffsetX-mainOffsetX, top+wingsOffsetPx+wingOffsetY+mainOffsetY, x+wingsOffsetPx+wingOffsetX+mainOffsetX, top+wingOffsetY-mainOffsetY, DISPLAY_COLOR);
  screen.drawLine(right-wingsOffsetPx-wingOffsetX-mainOffsetX, y-wingOffsetY+mainOffsetY, right-wingOffsetX+mainOffsetX, y-wingsOffsetPx-wingOffsetY-mainOffsetY, DISPLAY_COLOR);
  screen.drawLine(x+wingOffsetX-mainOffsetX, top+wingsOffsetPx+wingOffsetY+mainOffsetY, right-wingsOffsetPx-wingOffsetX-mainOffsetX, y-wingOffsetY+mainOffsetY, DISPLAY_COLOR);
  screen.drawLine(x+wingsOffsetPx+wingOffsetX+mainOffsetX, top+wingOffsetY-mainOffsetY, right-wingOffsetX+mainOffsetX, y-wingsOffsetPx-wingOffsetY-mainOffsetY, DISPLAY_COLOR);

  // Mirror
  screen.drawCircle(mirrorCenterX, mirrorCenterY, mirrorRadius, DISPLAY_COLOR);
}

void iconCourse(int x, int y, int iconSize, float value) {
  x += 10;
  y -= 1;
  int half = iconSize / 2;
  int tipOffset = iconSize / 3;

  int offsetX = x + half;
  int offsetY = y - half;
  double angle = value / 360 * 2 * PI;

  Point p1 = rot(angle, offsetX, offsetY, {x, y-half});
  Point p2 = rot(angle, offsetX, offsetY, {x+half, y-iconSize});
  screen.drawLine(p1.x, p1.y, p2.x, p2.y, DISPLAY_COLOR);
  p1 = rot(angle, offsetX, offsetY, {x+half, y-iconSize});
  p2 = rot(angle, offsetX, offsetY, {x+iconSize, y-half});
  screen.drawLine(p1.x, p1.y, p2.x, p2.y, DISPLAY_COLOR);
  p1 = rot(angle, offsetX, offsetY, {x+iconSize, y-half});
  p2 = rot(angle, offsetX, offsetY, {x+iconSize-tipOffset, y-half});
  screen.drawLine(p1.x, p1.y, p2.x, p2.y, DISPLAY_COLOR);
  p1 = rot(angle, offsetX, offsetY, {x+iconSize-tipOffset, y-half});
  p2 = rot(angle, offsetX, offsetY, {x+iconSize-tipOffset, y});
  screen.drawLine(p1.x, p1.y, p2.x, p2.y, DISPLAY_COLOR);
  p1 = rot(angle, offsetX, offsetY, {x+iconSize-tipOffset, y});
  p2 = rot(angle, offsetX, offsetY, {x+tipOffset, y});
  screen.drawLine(p1.x, p1.y, p2.x, p2.y, DISPLAY_COLOR);
  p1 = rot(angle, offsetX, offsetY, {x+tipOffset, y});
  p2 = rot(angle, offsetX, offsetY, {x+tipOffset, y-half});
  screen.drawLine(p1.x, p1.y, p2.x, p2.y, DISPLAY_COLOR);
  p1 = rot(angle, offsetX, offsetY, {x+tipOffset, y-half});
  p2 = rot(angle, offsetX, offsetY, {x, y-half});
  screen.drawLine(p1.x, p1.y, p2.x, p2.y, DISPLAY_COLOR);
}

void drawBarGauge (int x, int y, float fraction) {
  
  static TFT_eSprite sprite = TFT_eSprite(&screen);
  sprite.setColorDepth(16);
    
  x = 70;
  int width = 152;
  int height = 25;
  int lineWidth = 2;
  int lineAmountTotal = 25;
  int lineOffsetY = 3;

  int widthPerLine = (width - 2) / lineAmountTotal;
  int firstLineOffset = (widthPerLine - lineWidth) / 2 + 1;

  int linesNumber = floor(lineAmountTotal * fraction);

  sprite.createSprite(width, height);

  sprite.drawRect(0, 0, width, height, DISPLAY_COLOR);

  int offsetX = 1;
  for(int n = 0; n < linesNumber; n++) {
    for(int m = 0; m < lineWidth; m++) {
      int left = offsetX + n*widthPerLine + firstLineOffset + m;
      sprite.drawLine(left, lineOffsetY, left, height-lineOffsetY-1, DISPLAY_COLOR);
    }
  }

  sprite.pushSprite(x, y - height);

  sprite.deleteSprite();
  
}

void iconAngle(int x, int y, int iconSize, float value) {
  drawBarGauge(x, y, value/60);
}

void iconAcceleration(int x, int y, int iconSize, float value) {
  drawBarGauge(x, y, value);
}
