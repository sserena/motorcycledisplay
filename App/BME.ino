void bmeInit() {
  
  Wire.begin();

  while(!bme.begin())
  {
    Serial.println("Could not find BME280 sensor!");
    delay(1000);
  }

  switch(bme.chipModel())
  {
     case BME280::ChipModel_BME280:
       Serial.println("Found BME280 sensor! Success.");
       bmeInitialized = true;
       break;
     case BME280::ChipModel_BMP280:
       Serial.println("Found BMP280 sensor! No Humidity available.");
       bmeInitialized = true;
       break;
     default:
       Serial.println("Found UNKNOWN sensor! Error!");
  }
}

void bmeUpdate() {

  if (!miscCheckInterval("bme") || !bmeInitialized) {
    return;
  }

  float temp(NAN), hum(NAN), pres(NAN);
  bme.read(pres, temp, hum);

  displayPrint("temperature", temp);

  // The following is an attempt at calculating the altidue difference by means
  // barometer. Unfortunately, it seems to be even worse than GPS.
  /*
  // Altitude difference by pressure
  static double altitudePrevious = -1;
  static double altitudeDifference = 0;
  double altitudeCurrent = (pow(10, log10(pres / BME_SEA_LEVE_PRESSURE) / 5.2558797) - 1) / (-22.55769*0.000001);

  if (altitudePrevious > 0) {
    altitudeDifference += fabs(altitudeCurrent - altitudePrevious);
    displayPrint("altitude_total", altitudeDifference);
  }
  altitudePrevious = altitudeCurrent;
  */

  
  
  
}
