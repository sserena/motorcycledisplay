bool startupManager() {
  bool connectionEstablished = startupWifi();
  if (connectionEstablished) {
    printStatus("Wifi Connection Established");
    startupUpdate();
  }

  return true;
}

bool startupWifi() {
  
  WiFi.begin(WIFI_SSID, WIFI_PASSWORD);

  unsigned long start = millis();
  while (WiFi.status() != WL_CONNECTED && (millis() - start <= WIFI_WAIT_FOR_LOGIN_MS)) { // && (millis() - start <= WIFI_WAIT_FOR_LOGIN_MS)
    printStatus(".", false);
    delay(1000);
  }

  if (WiFi.status() == WL_CONNECTED) {
    return true;
  }
  return false;

}

// The remainder of this file attempts an OTA update.
// Unfortunately it doesn't work. My guess is some incompatibility with
// the specific ESP32 model I'm using because it worked on another one.
String startupUpdateGetHeaderValue(String header, String headerName) {
  return header.substring(strlen(headerName.c_str()));
}

void startupUpdate() {

  printStatus("Checking for updates...");

  int contentLength = 0;
  bool isValidContentType = false;
  
  WiFiClient client;
  
  if (client.connect(UPDATE_HOST.c_str(), UPDATE_PORT)) {

    // Request the contents of the bin file
    client.print(String("GET ") + UPDATE_PATH + " HTTP/1.1\r\n" +
                 "Host: " + UPDATE_HOST + "\r\n" +
                 "Cache-Control: no-cache\r\n" +
                 "Connection: close\r\n\r\n");

    // Connect to client to receive bin file
    unsigned long timeout = millis();
    while (client.available() == 0) {
      if (millis() - timeout > 5000) {
        printStatus("No Update");
        client.stop();
      }
    }

    // Read bin file
    while (client.available()) {
      String line = client.readStringUntil('\n');
      line.trim();

      // if the the line is empty,
      // this is end of headers
      // break the while and feed the
      // remaining `client` to the
      // Update.writeStream();
      if (!line.length()) {
        break; // and get the OTA started
      }

      // Check if the HTTP Response is 200
      // else break and Exit Update
      if (line.startsWith("HTTP/1.1")) {
        if (line.indexOf("200") < 0) {
          printStatus("Got a non 200 status code from server. Exiting OTA Update.");
          break;
        }
      }

      // extract headers here
      // Start with content length
      if (line.startsWith("Content-Length: ")) {
        contentLength = atoi((startupUpdateGetHeaderValue(line, "Content-Length: ")).c_str());
      }

      // Next, the content type
      if (line.startsWith("Content-Type: ")) {
        String contentType = startupUpdateGetHeaderValue(line, "Content-Type: ");
        if (contentType == "application/octet-stream") {
          isValidContentType = true;
        }
      }
    }
  } else {
    printStatus("Connection to " + String(UPDATE_HOST) + " failed. Please check your setup");
  }

  // check contentLength and content type
  if (contentLength && isValidContentType) {
    
    // Check if there is enough to OTA Update
    bool canBegin = Update.begin(contentLength);

    // If yes, begin
    if (canBegin) {
      printStatus("Begin OTA. This may take 2 - 5 mins to complete. Things might be quite for a while.. Patience!");
      size_t written = Update.writeStream(client);

      if (written == contentLength) {
        printStatus("Written : " + String(written) + " successfully");
      }

      if (Update.end()) {
        printStatus("OTA done!");
        if (Update.isFinished()) {
          printStatus("Update successfully completed. Rebooting.");
          ESP.restart();
        } else {
          printStatus("Update not finished? Something went wrong!");
        }
      } else {
        printStatus("Error Occurred. Error #: " + String(Update.getError()));
      }
    } else {
      printStatus("Not enough space to begin OTA");
      client.flush();
    }
  } else {
    printStatus("There was no content in the response");
    client.flush();
  }

}
